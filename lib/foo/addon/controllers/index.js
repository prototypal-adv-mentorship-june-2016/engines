import Ember from 'ember';

export default Ember.Controller.extend({
  interengine: Ember.inject.service(),

  actions: {
    doSomething() {
      this.set('interengine.helloWorld', 'hello from the foo engine');
    }
  }
});
